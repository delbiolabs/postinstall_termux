#!/usr/bin/env bash

#set -x

# Add explanation.

### OPTIONS AND VARIABLES ###

while getopts ":c:h:v" o; do case "${o}" in
	h) printf "Optional arguments for custom use:\\n  -c: a value from choices.csv file\\n\\n  -v to print all outputs" && exit 1 ;;
	c) chosen=${OPTARG} ;;
  v) verbose="x" ;;
	*) printf "Invalid option: -%s\\n" "$OPTARG" && exit 1 ;;
esac done

WIZARD_PREFIX="${WIZARD_PREFIX:-${HOME}/.local/src/dbs/postinstall_termux}"

menufile="${WIZARD_PREFIX}/choices.csv"
specdir="${WIZARD_PREFIX}/wrappers"
DBS_POSTINSTALL_PROGINSTALLER="${DBS_POSTINSTALL_PROGINSTALLER:-${WIZARD_PREFIX}/progs_installer}"
CURR_WIZARD="$0"
WIZARD_VERBOSE="$verbose"

export WIZARD_PREFIX
export DBS_POSTINSTALL_PROGINSTALLER
export CURR_WIZARD
export WIZARD_VERBOSE

tmpdir=$(mktemp -d)

[ -f "$menufile" ] && sed '/^#/d' "$menufile" > "$tmpdir/choices.csv"

user_choice() {

  # Construct menu file.  For some reasons, it's easier to constuct it on the fly
  # than use eval and such.
  echo "dialog --title \"DBS Debian Post-Install Wizard\" --menu \"What would you like to do?\" 15 45 8 \\" > $tmpdir/menu.sh
  echo $(cut -d, -f1,2 "$tmpdir/choices.csv" | sed -e "s/,/ /g")" \\" >> $tmpdir/menu.sh
  echo "2>$tmpdir/choice" >> $tmpdir/menu.sh

  # Get user input of what packages to install.
  bash "$tmpdir/menu.sh"
  chosen="$(cat "$tmpdir/choice")"

}

validate_chosen() {
 has_rec=$(cut -d, -f1 "$tmpdir/choices.csv" | grep -x "$chosen")
 [ -z "$has_rec" ] && chosen=""
}

[ -z "$chosen" ] && user_choice || validate_chosen

[[ $chosen == "" ]] && clear && exit

[[ -n "$verbose" ]] && clear

# In addition to installing the tagged programs, you can have scripts that run
# either before or after the installation.  To do this, you need only create a
# file in ~/.larbs-wizard/.specific/Z.pre (or Z.post).  `Z` here is the tag of
# the programs.

[[ -f  "$specdir/$chosen.pre" ]] && bash "$specdir/$chosen.pre"

# Quit script if preinstall script returned error or if user ended it.
#[[ ! $? -eq 0 ]] && clear &&  exit

[[ -z "$verbose" ]] && clear
# Run the `packerwrapper` script on all the programs tagged with the chosen tag
# in the progs file.
[[ -f "$specdir/$chosen.progs.csv" ]] && bash "$DBS_POSTINSTALL_PROGINSTALLER" "$specdir/$chosen.progs.csv"

# Post installation script.
[[ -f  "$specdir/$chosen.post" ]] && bash "$specdir/$chosen.post"
[[ -z "$verbose" ]] && clear
